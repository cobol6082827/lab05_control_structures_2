       IDENTIFICATION DIVISION.
       PROGRAM-ID. USER-DIV.
       AUTHOR. WARITPHAT

       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01  NUM1        PIC   9(5)     VALUE    ZEROES.
       01  NUM2        PIC   9(5)     VALUE    ZEROES.
           88 IS-ZERO  VALUE    ZEROES.
       01  RESULT      PIC   9(5)V9(3) VALUE    ZEROES.

       PROCEDURE DIVISION.
       000-BEGIN.
           PERFORM 001-USER-DIV THRU 001-EXIT
           GOBACK
       .
       
       001-USER-DIV.
           DISPLAY "Enter NUM1: " WITH NO ADVANCING
           ACCEPT NUM1
           DISPLAY "Enter NUM2: " WITH NO ADVANCING
           ACCEPT NUM2

           IF IS-ZERO THEN
              DISPLAY "NUM2 IS ZERO."
              GO TO 001-EXIT
           END-IF
           COMPUTE RESULT = NUM1 / NUM2
           DISPLAY "Result is " RESULT 
       .

       001-UESR-DIV-END.
           DISPLAY "USER-DIV IS END."
       .

       001-EXIT.
           EXIT.